# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/01/15 19:53:43 by almoraru          #+#    #+#              #
#    Updated: 2019/05/16 18:14:10 by fdubois          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc

SRC_DIR = srcs

OBJ_DIR = srcs

COLOR = \x1b[1;34m

COLOR2 = \x1b[1;35m

COLOR3 = \x1b[0;32m

COLOR4 = \x1b[0;36m

COLOREND = \033[0;0m

SRCS = $(SRC_DIR)/main.c $(SRC_DIR)/errors.c $(SRC_DIR)/png.c			 \
$(SRC_DIR)/png_filters.c $(SRC_DIR)/png_unzip.c $(SRC_DIR)/png_utils.c	\
$(SRC_DIR)/png_utils2.c  $(SRC_DIR)/parse.c $(SRC_DIR)/init.c			 \
$(SRC_DIR)/slav3d.c $(SRC_DIR)/game.c $(SRC_DIR)/sdl_events.c			 \
$(SRC_DIR)/ray_caster.c

NAME = wolf3d

FLAGS = -Wall -Wextra -Werror -g -fsanitize=address 

OBJECTS = $(SRCS:%.c=%.o)

LIB_PATH = libft/

SDL_LIB = ~/.brew/Cellar/sdl2/2.0.9_1/lib/

HEADER_PATH = includes/

INCLUDES = $(LIB_PARTH)/ $(HEADER_PATH)/

INCLUDE = -I$(LIB_PATH)/ -I$(HEADER_PATH)/

all: $(NAME)

$(OBJECTS): $(OBJ_DIR)/%.o : $(SRC_DIR)/%.c $(INCLUDES)
	@$(CC) `sdl2-config --cflags` -c $< -o $@ $(FLAGS) $(INCLUDE)

$(NAME): $(OBJECTS)
	@make -C libft
	@$(CC) -o $@ $^ $(FLAGS) -I includes/SDL2/ -L libft/ -lft $(INCLUDE) -framework opencl \
	-L $(SDL_LIB) `sdl2-config --libs` -lSDL2 -lSDL2_mixer -framework OpenGL -lz
	@echo "$(COLOR)Wolf3d : Blyat Royale has been successfully installed$(COLOREND)"
clean:
	@echo "$(COLOR3)Cleaning objects!$(COLOREND)"
	@make clean -C libft
	@rm -f $(OBJECTS)

fclean: clean
	@echo "$(COLOR)Removing :("
	@make fclean -C libft
	@rm -f $(NAME)
	@echo "$(COLOR4)You murderer!$(COLOREND)"

re: fclean all

.PHONY: all clean fclean re
