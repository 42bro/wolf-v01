/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   png.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/23 03:07:18 by fdubois           #+#    #+#             */
/*   Updated: 2019/05/20 16:13:20 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PNG_H
# define PNG_H

# include "../libft/libft.h"
# include "SDL2/SDL.h"
# include <stdlib.h>
# include <zlib.h>
# include <stdio.h>
# include <fcntl.h>
# include <math.h>

# define WX 800
# define WY 600

# define MAX_IDAT_SIZE 1048576
# define HAS_TRNS		(1 << 4)
# define HAS_GAMA		(1 << 3)

# define ERR_HDR		"Error reading header chunk data"
# define ERR_DEP		"Invalid / Unsupported bit depth ! (only 8 is good)"
# define ERR_INT		"PNG is interlaced (currently unsupported)"
# define ERR_F			"invalid filter byte"
# define ERR_FIL		"unsupported compression or filtering method"
# define ERR_G			"unsupported grayscale PNG"
# define ERR_CHK		"error reading useless chunk"
# define ERR_ZIP		"error reading compressed data"
# define ERR_UNZ		"error uncompressing data"
# define ERR_TRN		"invalid transparency data"

typedef struct	s_raw
{
	struct s_raw	*next;
	unsigned char	*data;
}				t_raw;

typedef struct	s_mlx
{
	void	*mlx;
	void	*win;
	void	*img;
	int		*pix;
}				t_mlx;

typedef struct	s_img
{
	int			*plte;
	SDL_Surface	*surf;
	size_t		usize;
	uint32_t	trns;
	uint32_t	gama;
	uint32_t	physx;
	uint32_t	physy;
	uint32_t	w;
	uint32_t	h;
	uint32_t	linesize;
	uint32_t	fd;
	uint8_t		plte_nb;
	uint8_t		depth;
	uint8_t		color_type;
	uint8_t		compression;
	uint8_t		filter;
	uint8_t		interlaced;
	uint8_t		channels;
	uint8_t		bpp;
	uint8_t		chunkflags;
	uint8_t		phys_spec; //pHYs unit : if not 0, pHYs info gives pixels per meter (if 0, unit unknown, pHYs gives aspect ratio
}				t_img;

void	unfilter(unsigned char *str, int i, t_img *img, uint8_t filter_type);
int		read_extra_chunk(unsigned char *tmp, int fd);
void			sub_unfilter(unsigned char *str, uint32_t i, t_img *img);
void			up_unfilter(unsigned char *str, uint32_t i, t_img *img);
void			average_unfilter(unsigned char *str, uint32_t i, t_img *img);
void			paeth_unfilter(unsigned char *str, uint32_t i, t_img *img);
uint32_t		chunk_len(unsigned char *tmp);
int				crc_check(int fd, unsigned char *tmp);
int				read_for_nuthin(int fd, uint32_t len);
t_img			png_header_check(int fd);
t_img			get_png_header_info(int fd, int len);
void			gros_fail(t_img *img, char *str);
void			get_gama_info(t_img *img, unsigned char *tmp, int fd);
void			get_phys_info(t_img *img, unsigned char *tmp, int fd);
void			get_palette_info(t_img *img, unsigned char *str, int fd);
int				invalid_trns_chunk(t_img *img, int len);
void			get_trns_info(t_img *img, unsigned char *tmp, int fd);
int				is_supported_anciliary_chunk(unsigned char *tmp);
void			get_anciliary_info(t_img *img, unsigned char *tmp, int fd);
unsigned char	*inflator(unsigned char *raw, uint32_t chunk_len, size_t usize);
int				unzip_img_data(unsigned char *raw_data, uint32_t chunk_len, t_img *img);
unsigned char	*zipped_data(unsigned char *tmp, int fd, uint32_t *comp_length);
void			put_pixel(uint32_t	*pixel, unsigned char *str, size_t k, t_img *img);
void			get_palette_info(t_img *img, unsigned char *str, int fd);

#endif
