/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/05 16:56:09 by fdubois           #+#    #+#             */
/*   Updated: 2019/05/29 20:24:48 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H

# include "../libft/libft.h"
# include "png.h"
# include <math.h>
# include <stdio.h>
# include <SDL2/SDL.h>
# include <SDL2/SDL_opengl.h>
# include <SDL2/SDL_audio.h>
# include <SDL2/SDL_mixer.h>
# include <pthread.h>

# define WIN_WIDTH 840//1024
# define WIN_HEIGHT 640//800

/*
**	Utils
*/

# define TRUE				1
# define FALSE				0
# define EPS				0.00000001
# define NB_SPRITES 2
# define TEX_W 64
# define TEX_H 64
# define U_DIV 1
# define V_DIV 1
# define V_MOVE 1
# define TEXTURE_ADDR		0x55AA
# define RGB_TO_ARGB(R, G, B)	(0xFF000000 | ((R) << 16) | ((G) << 8) | (B))
# define DARKEN_COLOR(C)     ((((C) >> 1) & 0x7F7F7F7F) | 0xFF000000)
# define CEILING_COLOR  RGB_TO_ARGB(0x21, 0x42, 0xCC)
# define FLOOR_COLOR    0x600A0A
# define NORTH             2
# define EAST             0
# define SOUTH             3
# define WEST             1

/*
**	MAP DEFINES
*/

# define P            -1
# define R             1
# define G             2
# define B             3
# define W             4
# define A             5
# define V             6
# define J             7
# define D             8
# define H             9
# define X             10
# define I             11
# define Z             12

/*

**	Music and Sound Effects
*/

# define NB_BGM		3
# define NB_SFX		8

# define BGM_ONE	0
# define BGM_TWO	1
# define BGM_RUS	2

# define SFX_MAP	0
# define SFX_AK		1
# define SFX_PEW	2
# define SFX_BL1	3
# define SFX_BL2	4
# define SFX_BL3	5
# define SFX_BL4	6
# define SFX_BL5	7

/*
 **	Player Movements (Bitfield)
 */

# define M_F	1
# define M_B	(1 << 1)
# define M_L	(1 << 2)
# define M_R	(1 << 3)
# define M_SL	(1 << 4)
# define M_SR	(1 << 5)
# define M_RUN	(1 << 6)

typedef struct	s_sprite
{
	double	x;
	double	y;
	Uint32	*texture;
}				t_sprite;

typedef struct	s_texture
{
	void		*pixel_data;
	SDL_Texture	*texture;
	Uint32		pitch;
	struct s_texture *next;
	struct s_texture *prev;
	Uint16	tag;
}				t_texture;

typedef struct	s_spr
{
	SDL_Surface	**sprite;
	int			nb;
}				t_spr;
typedef struct	s_game
{
	long	game_ticks;
	long	time;
	int	game;
}				t_game;

typedef struct	s_plane
{
	double	x;
	double	y;
}				t_plane;

typedef struct	s_pos
{
	double	x;
	double	y;
}				t_pos;

typedef struct	s_cam
{
	double	x;
	double	dir_x;
	double	dir_y;
}				t_cam;

typedef struct	s_ray
{
	double	dir_x;
	double	dir_y;
	//length of ray from current position to next x or y-side
	double	side_dist_x;
	double	side_dist_y;
	//length of ray from one x or y-side to next x or y-side
	double	delta_dist_x;
	double	delta_dist_y;
	int		hit;
	int		step_x;
	int		step_y;
}				t_ray;

typedef struct	s_map_pos
{
	int	x;
	int	y;
}				t_map_pos;

typedef struct	s_wall
{
	int		side;
	int		tex_x;
	int		tex_y;
	int		d;
	int		line_height;
	int		d_start;
	int		d_end;
	double	x;
}				t_wall;

typedef struct	s_floor
{
	double	x;
	double	y;
	double	weight;
	double	x_wall;
	double	y_wall;
	int		tex_x;
	int		tex_y;
}				t_floor;

typedef struct	s_sprite_utils
{
	double	x;
	double	y;
	double	trans_x;
	double	trans_y;
	double	inv_det;
	double	v_move;
	int		height;
	int		width;
	int		d;
	int		screen_x;
	int		v_move_screen;
	int		u_div;
	int		v_div;
	int		d_start_x;
	int		d_end_x;
	int		d_start_y;
	int		d_end_y;
	int		tex_x;
	int		tex_y;
}				t_sprite_utils;

typedef struct	s_env
{
	signed char	**map;
	Mix_Music	*bgm[NB_BGM];
	Mix_Chunk	*sfx[NB_SFX];
	SDL_Surface	*surface;
	SDL_Surface	*textures[8];
	SDL_Surface	*floor_tex[2];
	SDL_Surface	*sprite_tex[3];
	SDL_Window	*win;
	SDL_Event		event;
	SDL_Renderer	*renderer;
	Uint32		*screen_buf;
	double		z_buf[WIN_WIDTH];
	double	perp_dist;
	double	dist_wall;
	double	dist_player;
	double	current_dist;
	double	sprite_dist[NB_SPRITES];
	int		maph;
	int		mapw;
	int		tex_num;
	int		debug;
	int		sprite_order[NB_SPRITES];
	t_sprite	s[NB_SPRITES];
	t_sprite_utils	sp;
	t_texture	*managed_text;
	t_game		g;
	t_pos	pos;
	t_plane	plane;
	t_cam	cam;
	t_ray	ray;
	t_map_pos	m;
	t_floor	f;
	t_wall	wall;
}				t_env;

/*
** Init functions
*/

int		ft_init(t_env *e);
int		init_win(char *title, unsigned int width, unsigned int height
			  , t_env *e);
void	init_textures(t_env *e);
void	init_audio(t_env *e);

/*
** Parsing functinons
*/

void	parse(t_env *e, int ac, char **av);

/*
** Slav functions
*/

void	*create_texture(unsigned int width, unsigned int height, t_env *e);
void	clear_renderer(t_env *e);
void	present_renderer(t_env *e);
void	slav_draw_full_screen_texture(t_env *e, void *texture);
int		destroy_texture(void *p, t_env *e);

/*
** Util functions
*/

void		ft_error(char *s);
SDL_Surface	*png2surf(char *path);
void	run_game(t_env *e);
void	handle_sdl_events(t_env *e); 
void	ray_cast(t_env *e);

#endif
