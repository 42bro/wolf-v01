/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 18:52:20 by almoraru          #+#    #+#             */
/*   Updated: 2018/11/13 16:10:59 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *first;
	t_list *second;

	if (f == NULL)
		return (NULL);
	second = f(lst);
	first = second;
	if (first == NULL)
		return (NULL);
	while (lst->next != NULL)
	{
		lst = lst->next;
		second->next = f(lst);
		if (second->next == NULL)
			return (NULL);
		second = second->next;
	}
	return (first);
}
