/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 18:53:17 by almoraru          #+#    #+#             */
/*   Updated: 2018/11/16 18:59:51 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_realloc(void *ptr, size_t size)
{
	void	*tmp;

	tmp = NULL;
	if (ptr != NULL && !size)
	{
		tmp = ft_memalloc(1);
		if (tmp == NULL)
			return (NULL);
		ft_memdel(&ptr);
	}
	tmp = ft_memalloc(size);
	if (tmp == NULL)
		return (NULL);
	if (ptr != NULL)
	{
		ft_memcpy(tmp, ptr, size);
		ft_memdel(&ptr);
	}
	return (tmp);
}
