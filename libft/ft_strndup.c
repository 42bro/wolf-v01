/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/13 17:38:16 by almoraru          #+#    #+#             */
/*   Updated: 2018/11/13 17:41:53 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strndup(const char *src, size_t n)
{
	size_t	i;
	char	*dup;

	dup = ft_memalloc(ft_strlen(src) + 1);
	if (dup == NULL)
		return (NULL);
	i = 0;
	while (src[i] != '\0' && i < n)
	{
		dup[i] = src[i];
		i++;
	}
	while (i < n)
	{
		dup[i] = '\0';
		i++;
	}
	return (dup);
}
