/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 16:06:54 by almoraru          #+#    #+#             */
/*   Updated: 2018/11/09 15:55:51 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *haystack, const char *needle)
{
	unsigned int	i;
	unsigned int	j;
	const char		*src;
	const char		*find;

	if (needle[0] == '\0')
		return ((char*)haystack);
	src = haystack;
	find = needle;
	i = 0;
	while (src[i] != '\0')
	{
		if (src[i] == find[0])
		{
			j = 1;
			while (find[j] != '\0' && src[i + j] == find[j])
				j++;
			if (find[j] == '\0')
				return (&((char*)src)[i]);
		}
		i++;
	}
	return (NULL);
}
