/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/13 18:55:19 by almoraru          #+#    #+#             */
/*   Updated: 2018/12/03 14:41:43 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <unistd.h>
#include <stdlib.h>

static int	ft_handle_endbuff(char **line, char **str, const int fd, int size)
{
	if (size == BUFF_SIZE)
		return (get_next_line(fd, line));
	if (!(*line = ft_strdup(str[fd])))
		return (-1);
	ft_strdel(&str[fd]);
	return (0);
}

static int	ft_handle(char **line, char **str, const int fd, int size)
{
	int		i;
	char	*tmp;

	i = 0;
	while (str[fd][i] != '\0' && str[fd][i] != '\n')
		i++;
	if (str[fd][i] == '\n')
	{
		if (!(*line = ft_strsub(str[fd], 0, i)))
			return (-1);
		if (!(tmp = ft_strdup(str[fd] + i + 1)))
			return (-1);
		free(str[fd]);
		str[fd] = tmp;
		if (str[fd][0] == '\0')
			ft_strdel(&str[fd]);
	}
	else if (str[fd][i] == '\0')
		ft_handle_endbuff(line, str, fd, size);
	return (1);
}

static	int	ft_read_line(char **str, char **line, const int fd, int size)
{
	char	*tmp;
	char	buff[BUFF_SIZE + 1];

	while ((size = read(fd, buff, BUFF_SIZE)) > 0)
	{
		buff[size] = '\0';
		if (str[fd] == NULL)
			if (!(str[fd] = ft_strnew(1)))
				return (-1);
		if (!(tmp = ft_strjoin(str[fd], buff)))
			return (-1);
		ft_strdel(&str[fd]);
		str[fd] = tmp;
		if (ft_strchr(str[fd], '\n'))
			break ;
	}
	if (size < 0)
		return (-1);
	else if (size == 0 && (str[fd] == NULL || str[fd][0] == '\0'))
		return (0);
	else
		return (ft_handle(line, str, fd, size));
}

int			get_next_line(const int fd, char **line)
{
	static char		*str[225];
	int				size;

	size = 0;
	if (fd < 0 || line == NULL)
		return (-1);
	else
		return (ft_read_line(str, line, fd, size));
}
