/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c <rework/srcs>                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 11:38:22 by almoraru          #+#    #+#             */
/*   Updated: 2019/05/28 13:59:10 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	run_game(t_env *e)
{
	e->g.game_ticks = 0;
	while (e->g.game)
	{
		e->g.time = SDL_GetTicks();
		handle_sdl_events(e);
		ray_cast(e);
		SDL_Delay(10);
		if (!(e->g.game_ticks++ % 500))
			printf("FPS = %.2f\n", 1000.0f / (float)(SDL_GetTicks() - e->g.time));
	}
}
