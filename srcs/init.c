/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c <rework/srcs>                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 11:15:39 by almoraru          #+#    #+#             */
/*   Updated: 2019/05/29 20:12:16 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	init_audio(t_env *e)
{
	if (Mix_OpenAudio(44100, AUDIO_S16SYS, 2, 2048))
		ft_error((char*)Mix_GetError());
	e->bgm[BGM_ONE] = Mix_LoadMUS("sounds/music/banditradio.ogg");
	(!e->bgm[BGM_ONE] ? ft_error((char*)Mix_GetError()) : 1);
	e->bgm[BGM_TWO] = Mix_LoadMUS("sounds/music/blyatradio2.ogg");
	(!e->bgm[BGM_TWO] ? ft_error((char*)Mix_GetError()) : 1);
	e->bgm[BGM_RUS] = Mix_LoadMUS("sounds/music/blyatradio.ogg");
	(!e->bgm[BGM_RUS] ? ft_error((char*)Mix_GetError()) : 1);
	e->sfx[SFX_MAP] = Mix_LoadWAV("sounds/etc/whiz_01.wav");
	(!e->sfx[SFX_MAP] ? ft_error((char*)Mix_GetError()) : 1);
	e->sfx[SFX_AK] = Mix_LoadWAV("sounds/gun/ak47_draw.wav");
	(!e->sfx[SFX_AK] ? ft_error((char*)Mix_GetError()) : 1);
	e->sfx[SFX_PEW] = Mix_LoadWAV("sounds/gun/ak47_pew.wav");
	(!e->sfx[SFX_PEW] ? ft_error((char*)Mix_GetError()) : 1);
	Mix_AllocateChannels(64);
}

void	init_textures(t_env *e)
{
	e->textures[0] = png2surf("textures/pics/redbrick.png");
	e->textures[1] = png2surf("textures/pics/purplestone.png");
	e->textures[2] = png2surf("textures/pics/bluestone.png");
	e->textures[3] = png2surf("textures/pics/wood.png");
	e->textures[4] = png2surf("textures/pics/eagle.png");
	e->textures[5] = png2surf("textures/pics/greystone.png");
	e->textures[6] = png2surf("textures/pics/colorstone.png");
	e->textures[7] = png2surf("textures/pics/mossy.png");
	e->floor_tex[0] = png2surf("textures/pics/greystone.png");
	e->floor_tex[1] = png2surf("textures/pics/colorstone.png");
	e->sprite_tex[0] = png2surf("textures/pics/barrel.png");
	e->sprite_tex[1] = png2surf("textures/pics/pillar.png");
	e->sprite_tex[2] = png2surf("textures/pics/greenlight.png");
}

int		init_win(char *title, unsigned int width, unsigned int height
			  , t_env *e)
{
	if (e->win || e->renderer)
		return (FALSE);
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
		ft_error("Failed to initialize SDL");
	init_audio(e);
	e->win = SDL_CreateWindow(title, 0, 0, width, height, SDL_WINDOW_OPENGL);
	e->renderer = SDL_CreateRenderer(e->win, -1, SDL_RENDERER_ACCELERATED);
	if (!e->win || !e->renderer)
		ft_error("Failed to make window");
	SDL_SetWindowTitle(e->win, title);
	SDL_SetRenderDrawColor(e->renderer, 255, 255, 255, 255);
	SDL_RenderClear(e->renderer);
	SDL_RenderPresent(e->renderer);
	e->g.game = 1;
	return (TRUE);
}

int		ft_init(t_env *e)
{
	e->s[0].x = 9.0f;
	e->s[0].y = 3.0f;
	e->s[1].x = 16.0f;
	e->s[1].y = 8.0f;
	e->s[2].x = 7.0f;
	e->s[2].y = 21.0f;
	e->sp.u_div = 1;
	e->sp.v_div = 1;
	e->sp.v_move = 0.0f;
	e->managed_text = NULL;
	if (!init_win("SLAV 3D", WIN_WIDTH, WIN_HEIGHT, e))
		return (FALSE);
	if (!(e->screen_buf = create_texture(WIN_WIDTH, WIN_HEIGHT, e)))
		return (FALSE);
	init_textures(e);
	return (TRUE);
}
