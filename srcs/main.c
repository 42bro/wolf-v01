/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 10:28:32 by almoraru          #+#    #+#             */
/*   Updated: 2019/05/29 18:12:06 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		main(int ac, char **av)
{
	t_env e;

	ft_bzero(&e, sizeof(e));
	parse(&e, ac, av);
	if (!ft_init(&e))
		ft_error("Failed to initialize Blyat 3d!");
	e.pos.x = 7.0f;
	e.pos.y = 4.0f;
	e.plane.x = 0.0f;
	e.plane.y = 0.66f;
	e.cam.dir_x = -1.0f;
	e.cam.dir_y = 0.0f;
	e.debug = 0;
	run_game(&e);
	return (0);
}
