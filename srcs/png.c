/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   png.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/23 02:53:21 by fdubois           #+#    #+#             */
/*   Updated: 2019/05/20 16:13:23 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf3d.h"
#include "../includes/png.h"
#include <stdio.h>

void	gros_fail(t_img *img, char *str)
{
	ft_putendl(str);
	if (img->surf)
		SDL_FreeSurface(img->surf);
	if (img->plte)
		ft_memdel((void*)&(img->plte));
	close(img->fd);
	exit(1);
}

void	unfilter(unsigned char *str, int i, t_img *img, uint8_t filter_type)
{
	if (filter_type == 1)
		sub_unfilter(str, i, img);
	else if (filter_type == 2)
		up_unfilter(str, i, img);
	else if (filter_type == 3)
		average_unfilter(str, i, img);
	else if (filter_type == 4)
		paeth_unfilter(str, i, img);
}

int		read_extra_chunk(unsigned char *tmp, int fd)
{
	if (!(read_for_nuthin(fd, chunk_len(tmp))))
		return (0);
	if (!crc_check(fd, tmp) || read(fd, tmp, 8) != 8)
		return (0);
	return (1);
}

int		invalid_trns_chunk(t_img *img, int len)
{
	return ((img->color_type == 3 && len > img->plte_nb) || (img->color_type
== 2 && len != 3) || (!img->color_type && len != 2) || (img->color_type
== 4 || img->color_type == 6));
}

void	get_trns_info(t_img *img, unsigned char *tmp, int fd)
{
	int				i;
	int				len;
	unsigned char	c[6];

	len = chunk_len(tmp);
	i = -1;
	(invalid_trns_chunk(img, len) ? gros_fail(img, ERR_TRN) : 1);
	if (img->color_type == 3)
	{
		while (++i < img->plte_nb)
			if (read(fd, c, 1) != 1)
				gros_fail(img, ERR_TRN);
			else
				img->plte[i] |= (i < len ? (c[0] << 24) : (255 << 24));
	}
	else if (img->color_type == 2)
	{
		((read(fd, c, 6) != 6) ? gros_fail(img, ERR_TRN) : 1);
		img->trns = ((uint8_t)(*c)) << 24 | ((uint8_t)(*(c + 2))) << 16
| ((uint8_t)(*(c + 4))) << 8;
	}
	else if (!img->color_type)
	{
		((read(fd, c, 2) != 2) ? gros_fail(img, ERR_TRN) : 1);
		img->trns = ((uint16_t)(*c));
	}
}

t_img	get_png_header_info(int fd, int len)
{
	t_img			img;
	unsigned char	data[len];

	ft_bzero(&img, sizeof(img));
	if (read(fd, data, len) != len)
		gros_fail(&img, ERR_HDR);
	img.w = chunk_len(data);
	img.h = chunk_len(data + 4);
	(((img.depth = *(data + 8)) != 8) ? gros_fail(&img, ERR_DEP) : 1);
	img.color_type = *(data + 9);
	((img.color_type == 4 || img.color_type == 0) ? gros_fail(&img, ERR_G) : 1);
	img.compression = *(data + 10);
	img.filter = *(data + 11);
	((img.compression || img.filter) ? gros_fail(&img, ERR_FIL) : 1);
	img.interlaced = *(data + 12);
	(img.interlaced ? gros_fail(&img, ERR_INT) : 1);
	if (img.color_type == 6 || img.color_type == 4)
		img.channels = (img.color_type == 6 ? 4 : 2);
	else
		img.channels = (!img.color_type || img.color_type == 3 ? 1 : 3);
	img.bpp = (img.depth / 8) * img.channels;
	img.linesize = 1 + (img.w * img.bpp);
	img.usize = img.linesize * img.h;
	img.fd = fd;
	return (img);
}

int	import_png_to_surface(int fd, t_img *img)
{
	unsigned char	tmp[8];
	uint32_t		comp_length;
	unsigned char	*zip;

	if (!fd || !(img->surf->pixels) || read(fd, tmp, 8) < 8)
		return (0);
	while (ft_strncmp((const char*)(tmp + 4), "IDAT", 4))
	{
		if (is_supported_anciliary_chunk(tmp))
			get_anciliary_info(img, tmp, fd);
		else
			(!(read_for_nuthin(fd, chunk_len(tmp))) ? gros_fail(img, ERR_CHK) : 1);
		if (!crc_check(fd, tmp) || read(fd, tmp, 8) < 8)
			return (0);
	}
	if (ft_strncmp((const char*)(tmp + 4), "IDAT", 4))
		return (0);
	(!(zip = zipped_data(tmp, fd, &comp_length)) ? gros_fail(img, ERR_ZIP) : 1);
	if (ft_strncmp((const char*)(tmp + 4), "IEND", 4))
		return (0);
	if (!(unzip_img_data(zip, comp_length, img)))
		gros_fail(img, ERR_UNZ);
	ft_memdel((void**)&zip);
	close(fd);
	return (1);
}

SDL_Surface *png2surf(char *path)
{
	int fd;
	t_img img;

	ft_bzero(&img, sizeof(img));
	if (((fd = open(path, O_RDONLY)) == -1) || !path)
		perror("Couldn't open PNG file.");
	img = png_header_check(fd);
	if (!(img.surf = SDL_CreateRGBSurface(0, img.w, img.h, (img.depth * 4),
0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000)))
		perror(SDL_GetError());
	if (!(import_png_to_surface(fd, &img)))
	{
		gros_fail(&img, "problem importing PNG data");
		return (0);
	}
	if (img.plte)
		ft_memdel((void**)&(img.plte));
	return (img.surf);
}

void	copy_sprite(SDL_Surface *dst, SDL_Surface *src, int x, int y)
{
	int	i;

	i = -1;
	while (++i < dst->h)
		ft_memcpy((dst->pixels + (i * dst->pitch)), (src->pixels
+ (y * src->pitch) + x), dst->pitch);
}

t_spr	surf2sprite(SDL_Surface *surf, int nb, int x, int y)
{
	t_spr	spr;
	int		i;
	int		sx;
	int		sy;

	i = 0;
	sx = 0;
	sy = 0;
	if (!x || !y || nb < 2 || (x * y * nb) > (surf->pitch * surf->h))
		gros_fail(&(t_img){.surf = surf}, "invalid sprite dimensions");
	if (!(spr.sprite = (SDL_Surface**)malloc(sizeof(SDL_Surface*) * nb)))
		gros_fail(&(t_img){.surf = surf}, "malloc sucks !");
	spr.nb = nb;
	while (i < nb)
	{
		if (!(spr.sprite[i] = SDL_CreateRGBSurface(0, x, y, 32, 0x00FF0000,
0x0000FF00, 0x000000FF, 0xFF000000)))
			perror(SDL_GetError());
		copy_sprite(spr.sprite[i], surf, sx, sy);
		printf("sx %d sy %d w %d \n", sx, sy, surf->w);
		sx = (sx + x >= surf->w - 1 ? 0 : sx + x);
		sy = (!sx && (sy + y < surf->w) ? sy + y : sy);
		i++;
	}
	return (spr);
}
