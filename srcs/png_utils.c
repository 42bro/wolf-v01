/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   png_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/04 15:45:18 by fdubois           #+#    #+#             */
/*   Updated: 2019/05/20 15:52:44 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/png.h"

uint32_t	chunk_len(unsigned char *tmp)
{
	int i;

	i = 0;
	return ((tmp[i] << 24 | tmp[(i + 1)] << 16 | tmp[(i + 2)] << 8
| tmp[(i + 3)]));
}

int			crc_check(int fd, unsigned char *tmp)
{
	if (read(fd, tmp, 4) < 4)
		return (0);
	return (1);
}

int			read_for_nuthin(int fd, uint32_t len)
{
	//for chunks we dont care about
	unsigned char	tmp[len];

	if (read(fd, tmp, len) < len)
		return (0);
	return (1);
}

t_img	png_header_check(int fd)
{
	unsigned char tmp[8];
	t_img img;

	if (read(fd, tmp, 8) < 8 || ft_strncmp((const char*)tmp, 
"\x89\x50\x4E\x47\x0D\x0A\x1A\x0A", 8))
		gros_fail(&img, "Not a PNG.");
	if (read(fd, tmp, 8) < 8 || ft_strncmp((const char*)(tmp + 4), "IHDR", 4))
		gros_fail(&img, "Missing or invalid header chunk");
	img = get_png_header_info(fd, chunk_len(tmp));
	if (!crc_check(fd, tmp))
		gros_fail(&img, "Error checking CRC");
	return (img);
}
