/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_caster.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 11:58:31 by almoraru          #+#    #+#             */
/*   Updated: 2019/05/29 20:24:54 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	sprite_swap2(double *a, double *b)
{
	double tmp;

	tmp = *a;
	*a = *b;
	*b = tmp;
}

void	sprite_swap(int *a, int *b)
{
	int tmp;

	tmp = *a;
	*a = *b;
	*b = tmp;
}

void	comb_sort(int *order, double *dist, int amount)
{
	int gap;
	int swap;
	int i;
	int j;

	gap = amount;
	swap = FALSE;
	while (gap > 1 || swap)
	{
		gap = (gap * 10) / 13;
		if (gap == 9 || gap == 10)
			gap = 11;
		if (gap < 1)
			gap = 1;
		swap = FALSE;
		i = -1;
		while (++i < amount - gap)
		{
			j = i + gap;
			if (dist[i] < dist[j])
			{
				sprite_swap2(&dist[i], &dist[j]);
				sprite_swap(&order[i], &order[j]);
				swap = TRUE;
			}
		}
	}
}

void	draw_sprites(t_env *e, int i, int stripe)
{
	int y;
	Uint32 color;

	y = e->sp.d_start_y - 1;
	while (++y < e->sp.d_end_y)
	{
		e->sp.d = (y - e->sp.v_move_screen) * 256 - WIN_HEIGHT * 128
			+ e->sp.height * 128;
		e->sp.tex_y = ((e->sp.d * e->sprite_tex[0]->h) / e->sp.height) / 256;
		e->s[i].texture = e->sprite_tex[i]->pixels;
		color = e->s[i].texture[TEX_W * e->sp.tex_y + e->sp.tex_x];
		if ((color & 0x00FFFFFF) != 0)
			e->screen_buf[y * WIN_WIDTH + stripe] = color;
	}
}

void	draw_sprites_width(t_env *e, int i)
{
	int stripe;

	e->sp.width = abs((int)(WIN_HEIGHT / (e->sp.trans_y))) / e->sp.u_div;
	e->sp.d_start_x = -e->sp.width / 2 + e->sp.screen_x;
	if (e->sp.d_start_x < 0)
		e->sp.d_start_x = 0;
	e->sp.d_end_x = e->sp.width /2 + e->sp.screen_x;
	if (e->sp.d_end_x >= WIN_WIDTH)
		e->sp.d_end_x = WIN_WIDTH - 1;
	stripe = e->sp.d_start_x - 1;
	while (++stripe < e->sp.d_end_x)
	{
		e->sp.tex_x = (int)(256 * (stripe - (-e->sp.width / 2 + e->sp.screen_x))
							* e->sprite_tex[0]->w / e->sp.width) / 256;
		if (e->sp.trans_y > 0 && stripe > 0 && stripe < WIN_WIDTH
			&& e->sp.trans_y < e->z_buf[stripe])
			draw_sprites(e, i, stripe);
	}
}

void	draw_sprites_height(t_env *e, int i)
{
	e->sp.v_move_screen = (int)(e->sp.v_move / e->sp.trans_y);
	e->sp.height = abs((int)(WIN_HEIGHT / (e->sp.trans_y))) / e->sp.v_div;
	e->sp.d_start_y = -e->sp.height / 2 + WIN_HEIGHT / 2 + e->sp.v_move_screen;
	if (e->sp.d_start_y < 0)
		e->sp.d_start_y = 0;
	e->sp.d_end_y = e->sp.height / 2 + WIN_HEIGHT / 2 + e->sp.v_move_screen;
	if (e->sp.d_end_y >= WIN_HEIGHT)
		e->sp.d_end_y = WIN_HEIGHT - 1;
	draw_sprites_width(e, i);
}

void	sort_and_prep_sprites(t_env *e)
{
	int i;
	int j;

	i = -1;
	while (++i < NB_SPRITES)
	{
		e->sprite_order[i] = i;
		e->sprite_dist[i] = ((e->pos.x - e->s[i].x) * (e->pos.x - e->s[i].x)
							 + (e->pos.y - e->s[i].y) * (e->pos.y - e->s[i].y));
	}
	comb_sort(e->sprite_order, e->sprite_dist, NB_SPRITES);
	j = -1;
	while (++j < NB_SPRITES)
	{
		e->sp.x = e->s[e->sprite_order[i]].x - e->pos.x;
		e->sp.y = e->s[e->sprite_order[i]].y - e->pos.y;
		e->sp.inv_det = 1.0f / (e->plane.x * e->cam.dir_y 
								- e->cam.dir_x * e->plane.y);
		e->sp.trans_x = e->sp.inv_det * (e->cam.dir_y * e->sp.x
										 - e->cam.dir_x *e->sp.y);
		e->sp.trans_y = e->sp.inv_det * (-e->plane.y * e->sp.x
										 + e->plane.x * e->sp.y);
		e->sp.screen_x = (int)((WIN_WIDTH / 2)
							   * (1 + e->sp.trans_x / e->sp.trans_y));
		draw_sprites_height(e, j);
	}
}

void	draw_floor(t_env *e, int x)
{
	Uint32 *p;
	Uint32 color;
	int y;

	e->dist_player = 0.0f;
	e->dist_wall = e->perp_dist;
	if (e->wall.d_end < 0)
		e->wall.d_end = WIN_HEIGHT;
	y = e->wall.d_end - 1;
	while (++y < WIN_HEIGHT)
	{
		e->current_dist = WIN_HEIGHT / (2.0f * y - WIN_HEIGHT);
		e->f.weight = (e->current_dist - e->dist_player) / (e->dist_wall - e->dist_player);
		e->f.x = e->f.weight * e->f.x_wall + (1.0f - e->f.weight) * e->pos.x;
		e->f.y = e->f.weight * e->f.y_wall + (1.0f - e->f.weight) * e->pos.y;
		e->f.tex_x = (int)(e->f.x * e->floor_tex[0]->w) % e->floor_tex[0]->w;
		e->f.tex_y = (int)(e->f.y * e->floor_tex[0]->h) % e->floor_tex[0]->h;
		p = e->floor_tex[0]->pixels;
		if (e->debug)
		{
			printf("tex x = %d | tex y = %d\n", e->f.tex_x, e->f.tex_y);
			printf("pos x = %f | pos y = %f\n", e->pos.x, e->pos.y);
		}
		if (e->f.tex_y < 0)
			e->f.tex_y = 0;
		color = p[TEX_W * e->f.tex_y + e->f.tex_x];
		e->screen_buf[y * WIN_WIDTH + x] = color;
		p = e->floor_tex[1]->pixels;
		color = p[TEX_W * e->f.tex_y + e->f.tex_x];
		e->screen_buf[(WIN_HEIGHT - y) * WIN_WIDTH + x] = color;
	}
}

void	prepare_floor_casting(t_env *e)
{
	if (e->wall.side == 0 && e->ray.dir_x > 0)
	{
		e->f.x_wall = e->m.x;
		e->f.y_wall = e->m.y + e->wall.x;
	}
	else if (e->wall.side == 0 && e->ray.dir_x < 0)
	{
		e->f.x_wall = e->m.x + 1.0f;
		e->f.y_wall = e->m.y + e->wall.x;
	}
	else if (e->wall.side == 1 && e->ray.dir_y > 0)
	{
		e->f.x_wall = e->m.x + e->wall.x;
		e->f.y_wall = e->m.y;
	}
	else
	{
		e->f.x_wall = e->m.x + e->wall.x;
		e->f.y_wall = e->m.y + 1.0f;
	}
}

void	draw_walls(t_env *e, int x)
{
	Uint32 *p;
	Uint32 color;
	int y;

	y = e->wall.d_start - 1;
	while (++y < e->wall.d_end)
	{
		e->wall.d = y * 256 - WIN_HEIGHT * 128 + e->wall.line_height * 128;
		e->wall.tex_y = ((e->wall.d * TEX_H) / e->wall.line_height) / 256;
		if (e->wall.side == 1)
		{
			p = e->textures[3]->pixels;
			color = p[TEX_H * e->wall.tex_y + e->wall.tex_x];
		}
		else if (e->wall.side == 0)
		{
			p = e->textures[1]->pixels;
			color = p[TEX_H * e->wall.tex_y + e->wall.tex_x];
		}
		else if (e->wall.side == 1 && e->ray.dir_x < 0)
		{
			p = e->textures[3]->pixels;
			color = p[TEX_H * e->wall.tex_y + e->wall.tex_x];
		}
		else if (e->wall.side == 0 && e->ray.dir_y < 0)
		{
			p = e->textures[5]->pixels;
			color = p[TEX_H * e->wall.tex_y + e->wall.tex_x];
		}
		color = DARKEN_COLOR(color);
		e->screen_buf[y * WIN_WIDTH + x] = color;
	}
}

void	prepare_draw_2(t_env *e)
{
	e->wall.tex_x = (int)(e->wall.x * (double)(TEX_W));
	if (e->wall.side == 0 && e->ray.dir_x > 0)
		e->wall.tex_x = TEX_W - e->wall.tex_x - 1;
	else if (e->wall.side == 1 && e->ray.dir_y < 0)
		e->wall.tex_x = TEX_W - e->wall.tex_x - 1;
}

void	prepare_draw(t_env *e)
{
	if (e->wall.side == 0)
		e->perp_dist = (e->m.x - e->pos.x + (1.0f - e->ray.step_x) / 2.0f)
		/ e->ray.dir_x;
	else
		e->perp_dist = (e->m.y - e->pos.y + (1.0f - e->ray.step_y) / 2.0f)
		/ e->ray.dir_y;
	e->wall.line_height = (int)(WIN_HEIGHT / e->perp_dist);
	e->wall.d_start = -e->wall.line_height / 2 + WIN_HEIGHT / 2;
	if (e->wall.d_start < 0)
		e->wall.d_start = 0;
	e->wall.d_end = e->wall.line_height / 2 + WIN_HEIGHT / 2;
	if (e->wall.d_end >= WIN_HEIGHT)
		e->wall.d_end = WIN_HEIGHT - 1;
	e->tex_num = e->map[e->m.x][e->m.y] - 1;
	if (e->wall.side == 0)
		e->wall.x = e->pos.y + e->perp_dist * e->ray.dir_y;
	else
		e->wall.x = e->pos.x + e->perp_dist * e->ray.dir_x;
	e->wall.x -= floor((e->wall.x));
	prepare_draw_2(e);
}

void	calculate_initial_side_dist(t_env *e)
{
	if (e->ray.dir_x < 0)
	{
		e->ray.step_x = -1;
		e->ray.side_dist_x = (e->pos.x - e->m.x) * e->ray.delta_dist_x;
	}
	else
	{
		e->ray.step_x = 1;
		e->ray.side_dist_x = (e->m.x + 1.0f - e->pos.x) * e->ray.delta_dist_x;
	}
	if (e->ray.dir_y < 0)
	{
		e->ray.step_y = -1;
		e->ray.side_dist_y = (e->pos.y - e->m.y) * e->ray.delta_dist_y;
	}
	else
	{
		e->ray.step_y = 1;
		e->ray.side_dist_y = (e->m.y + 1.0f - e->pos.y) * e->ray.delta_dist_y;
	}
}

void	extend_ray_until_hit(t_env *e)
{
	while (e->ray.hit == 0)
	{
		if (e->ray.side_dist_x < e->ray.side_dist_y)
		{
			e->ray.side_dist_x += e->ray.delta_dist_x;
			e->m.x += e->ray.step_x;
			e->wall.side = 0;
		}
		else
		{
			e->ray.side_dist_y += e->ray.delta_dist_y;
			e->m.y += e->ray.step_y;
			e->wall.side = 1;
		}
		if (e->map[e->m.x][e->m.y] > 0)
			e->ray.hit = TRUE;
	}
}

void	ray_cast(t_env *e)
{
	int x;

	x = -1;
	while (++x < WIN_WIDTH)
	{
		e->cam.x = 2 * x / (double)(WIN_WIDTH) - 1;
		e->ray.dir_x = e->cam.dir_x + e->plane.x * e->cam.x;
		e->ray.dir_y = e->cam.dir_y + e->plane.y * e->cam.x;
		e->m.x = (int)(e->pos.x);
		e->m.y = (int)(e->pos.y);
		e->ray.delta_dist_x = fabs(1.0f / e->ray.dir_x);
		e->ray.delta_dist_y = fabs(1.0f / e->ray.dir_y);
		e->ray.hit = 0;
		calculate_initial_side_dist(e);
		extend_ray_until_hit(e);
		prepare_draw(e);
		draw_walls(e, x);
		e->z_buf[x] = e->perp_dist;
		prepare_floor_casting(e);
		draw_floor(e, x);
		sort_and_prep_sprites(e);
	}
	clear_renderer(e);
	slav_draw_full_screen_texture(e, e->screen_buf);
}
