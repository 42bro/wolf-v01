/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdl_events.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 11:41:11 by almoraru          #+#    #+#             */
/*   Updated: 2019/05/29 17:49:29 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	handle_sdl_events(t_env *e)
{
	double old_dir_x;
	double old_plane_x;

	while ((SDL_PollEvent(&e->event) != 0))
	{
		if (e->event.type == SDL_KEYDOWN)
		{
			if (e->event.key.keysym.sym == SDLK_ESCAPE)
			{
				if (Mix_PlayingMusic())
					Mix_HaltMusic();
				Mix_FreeChunk(e->sfx[0]);
				Mix_FreeMusic(e->bgm[0]);
				Mix_Quit();
				e->g.game = FALSE;
			}
			if (e->event.key.keysym.sym == SDLK_w)
			{
				if (e->map[(int)(e->pos.x + e->cam.dir_x)][(int)(e->pos.y)] == FALSE)
					e->pos.x += e->cam.dir_x;
				if (e->map[(int)(e->pos.x)][(int)(e->pos.y + e->cam.dir_y)] == FALSE)
					e->pos.y += e->cam.dir_y;
			}
			if (e->event.key.keysym.sym == SDLK_s)
			{
				if (e->map[(int)(e->pos.x - e->cam.dir_x * 1)][(int)(e->pos.y)] == FALSE)
					e->pos.x -= e->cam.dir_x;
				if (e->map[(int)(e->pos.x)][(int)(e->pos.y - e->cam.dir_y * 1)] == FALSE)
					e->pos.y -= e->cam.dir_y;
			}
			if (e->event.key.keysym.sym == SDLK_d)
			{
				old_dir_x = e->cam.dir_x;
				e->cam.dir_x = e->cam.dir_x * cos(-0.1f) - e->cam.dir_y * sin(-0.1f);
				e->cam.dir_y = old_dir_x * sin(-0.1f) + e->cam.dir_y * cos(-0.1f);
				old_plane_x = e->plane.x;
				e->plane.x = e->plane.x * cos(-0.1f) - e->plane.y * sin(-0.1f);
				e->plane.y = old_plane_x * sin(-0.1f) + e->plane.y * cos(-0.1f);
			}
			if (e->event.key.keysym.sym == SDLK_a)
			{
				old_dir_x = e->cam.dir_x;
				e->cam.dir_x = e->cam.dir_x * cos(0.1f) - e->cam.dir_y * sin(0.1f);
				e->cam.dir_y = old_dir_x * sin(0.1f) + e->cam.dir_y * cos(0.1f);
				old_plane_x = e->plane.x;
				e->plane.x = e->plane.x * cos(0.1f) - e->plane.y * sin(0.1f);
				e->plane.y = old_plane_x * sin(0.1f) + e->plane.y * cos(0.1f);
			}
			if (e->event.key.keysym.sym == SDLK_t)
				e->debug = 1;
			if (e->event.key.keysym.sym == SDLK_g)
				e->debug = 0;}
		else if (e->event.type == SDL_QUIT)
		{
			if (Mix_PlayingMusic())
				Mix_HaltMusic();
			Mix_FreeChunk(e->sfx[0]);
			Mix_FreeMusic(e->bgm[0]);
			Mix_Quit();
			e->g.game = FALSE;
		}
	}
}
