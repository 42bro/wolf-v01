/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   slav3d.c <rework/srcs>                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 11:20:12 by almoraru          #+#    #+#             */
/*   Updated: 2019/05/28 15:10:53 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	slav_add_list(Uint32 *data, t_texture *tex, t_env *e)
{
	*((t_texture**)data) = tex;
	tex->pixel_data = ((t_texture**)data) + 1;
	if (e->managed_text)
	{
		tex->next = e->managed_text;
		e->managed_text->prev = tex;
	}
	e->managed_text = tex;
}

void	*create_texture(unsigned int width, unsigned int height, t_env *e)
{
	Uint32		*data;
	t_texture	*tex;

	if (!width || !height || !e->renderer)
		ft_error("SDL hasn't been initliazed yet!");
	tex = (t_texture *)malloc(sizeof(t_texture));
	tex->pitch = width * sizeof(Uint32);
	tex->next = NULL;
	tex->prev = NULL;
	tex->tag = TEXTURE_ADDR;
	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1");
	if (!(tex->texture = SDL_CreateTexture(e->renderer, SDL_PIXELFORMAT_ARGB8888
										   ,SDL_TEXTUREACCESS_STREAMING
										   ,width ,height)))
	{
		free(tex);
		ft_error("Failed to make texture");
	}
	data = (Uint32 *)malloc((sizeof(Uint32) * width * height)
							+ sizeof(t_texture *));
	slav_add_list(data, tex, e);
	return (tex->pixel_data);
}


void	clear_renderer(t_env *e)
{
	SDL_RenderClear(e->renderer);
}

int		destroy_texture(void *p, t_env *e)
{
	t_texture *t;

	t = *(((t_texture**)p) - 1);
	if (t->tag != TEXTURE_ADDR)
	{
		write(1, "Not a valid texture pointer", 28);
		return (0);
	}
	t->tag = 0;
	free(((t_texture**)p) - 1);
	SDL_DestroyTexture(t->texture);
	if (t->prev)
		t->prev->next = t->next;
	if (t->next)
		t->next->prev = t->prev;
	if (e->managed_text == t)
		e->managed_text = t->next;
	free(t);
	return (1);
}

void	present_renderer(t_env *e)
{
	SDL_RenderPresent(e->renderer);
}

void	slav_draw_full_screen_texture(t_env *e, void *texture)
{
	t_texture *t;

	if (!e->win || !e->renderer)
		ft_error("SDL window has not beed initialized yet");
	t = *(((t_texture**)texture) - 1);
	if (t->tag != TEXTURE_ADDR)
		ft_error("Not a valid texture pointer");
	SDL_UpdateTexture(t->texture, NULL, t->pixel_data, t->pitch);
	clear_renderer(e);
	SDL_RenderCopy(e->renderer, t->texture, NULL, NULL);
	present_renderer(e);
}
